<div class ="container" >
<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      
            <a class="navbar-brand" href="#">PERPUSTAKAAN DESA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
               
                  <a class="nav-link" href="{{ route('books.index')}}">Books <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('borrowers.index')}}">Borrowers <span class="sr-only">(current)</span></a>
                      </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('books.index')}}">Brand <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="{{ route('books.index')}}">Category <span class="sr-only">(current)</span></a>
                  </li>
				 </nav>

        </div>
</div>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Edit Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <body>
    <div class="container">
      <h2>Edit Books Information</h2>
      <br />
      <form method="post" action="{{action('BooksController@update', $books->id)}}">
        @csrf
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="bookid">Book ID:</label>
            <input type="text" class="form-control" name="bookid" value="{{$books->bookid}}">
          </div>
        </div>
		<div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="title">Title:</label>
            <input type="text" class="form-control" name="title" value="{{$books->title}}">
          </div>
        </div>
		<div class="row">
		<div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="author">Author</label>
             <input type="text" class="form-control" name="author" value="{{$books->author}}">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="publishername">Publisher Name:</label>
             <input type="text" class="form-control" name="publishername" value="{{$books->publishername}}">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="publishedyear">Publisher Year:</label>
             <input type="integer" class="form-control" name="publishedyear" value="{{$books->publishedyear}}">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="category">Category:
             </label>
             <input type="text" class="form-control" name="category" value="{{$books->category}}">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
        
          <div class="form-group col-md-4" style="margin-top:60px">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Update</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>

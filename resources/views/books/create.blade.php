<div class ="container" >
<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      
            <a class="navbar-brand" href="#">PERPUSTAKAAN DESA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
               
                  <a class="nav-link" href="{{ route('books.index')}}">Books <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('borrowers.index')}}">Borrowers <span class="sr-only">(current)</span></a>
                      </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('books.index')}}">Brand <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="{{ route('books.index')}}">Category <span class="sr-only">(current)</span></a>
                  </li>
				 </nav>

        </div>
</div>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Add New books</h2>
      <br/>
      <form method="post" action="{{url('books')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="bookid">Book ID</label>
             <input type="text" class="form-control" name="bookid" placeholder="e.g. B123">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="title">Title</label>
             <input type="text" class="form-control" name="title" placeholder="e.g. Anfield">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="author">Author</label>
             <input type="text" class="form-control" name="author" placeholder="e.g. Alwin">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="publishername">Publisher Name:</label>
             <input type="text" class="form-control" name="publishername" placeholder="e.g. Atok">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="publishedyear">Publisher Year:</label>
             <input type="integer" class="form-control" name="publishedyear" placeholder="e.g. 2018">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="category">Category:
             </label>
             <input type="text" class="form-control" name="category" placeholder="e.g. Agama">
           </div>
         </div>
         <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4" style="margin-top:60px">
             <button type="submit" class="btn btn-success">Submit
             </button>
           </div>
         </div>
       </form>
     </div>
    <body>
	
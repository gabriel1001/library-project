<div class ="container" >
<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      
            <a class="navbar-brand" href="#">Perpustakaan Desa</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
               
                  <a class="nav-link" href="{{ route('books.index')}}">Books <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('borrowers.index')}}">Borrowers <span class="sr-only">(current)</span></a>
                      </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('books.index')}}">Brand <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="{{ route('books.index')}}">Category <span class="sr-only">(current)</span></a>
                  </li>
				 </nav>

        </div>
</div>
 
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Borrower Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <table class="table table-striped">
      <thead>
      <tr>
        <th>No</th>
        <th>Name</th>
        <th>IC Number</th>
		<th>Address</th>
        <th>Phone Number</th>
		
		
        <th colspan=3>Action</th>
      </tr>
    </thead>
<a href="{{ route('borrowers.create')}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Add  Borrower</a>
    @php
      $i=1;
    @endphp
    @foreach($borrowers as $borrowers)
      <tr>
        <td>@php echo $i++; @endphp</td>
		<td>{{$borrowers->name}}</td>
        <td>{{$borrowers->icnumber}}</td>
        <td>{{$borrowers->address}}</td>
		<td>{{$borrowers->phonenumber}}</td>
        <td><a href="{{action('BorrowerControllerr@edit', $borrowers->id)}}" class="btn btn-warning">Edit</a></td>&nbsp;
          <form action="{{action('BorrowerControllerr@destroy', $borrowers->id)}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <td><button class="btn btn-danger" type="submit" onclick="return confirm('Are you sure?')">Delete</button></td>
            @endforeach
          </form>
        </td>
      </td>
    </tr>
  </table>

  </body>
</html>


<div class ="container" >
<!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      
            <a class="navbar-brand" href="#">ALWIN SPORTS</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav mr-auto">
               
                  <a class="nav-link" href="{{ route('books.index')}}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ route('borrowers.index')}}">Product <span class="sr-only">(current)</span></a>
                      </li>
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('books.index')}}">Brand <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="{{ route('books.index')}}">Category <span class="sr-only">(current)</span></a>
                  </li>
				 </nav>

        </div>
</div>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Add New Borrowers</h2>
      <br/>
      <form method="post" action="{{url('borrowers')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="Name">Name :</label>
             <input type="text" class="form-control" name="name" placeholder="e.g. Alwin">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="icnumber">IC Number :</label>
             <input type="text" class="form-control" name="icnumber" placeholder="e.g. 951130106131">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="address">Address :</label>
             <input type="text" class="form-control" name="address" placeholder="e.g. Klang">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
             <label for="phonenumber">Phone Number:</label>
             <input type="integer" class="form-control" name="phonenumber" placeholder="e.g. 0163132415">
           </div>
         </div>
		 <div class="row">
           <div class="col-md-4">
           </div>
           <div class="form-group col-md-4">
        
           <div class="form-group col-md-4" style="margin-top:60px">
             <button type="submit" class="btn btn-success">Submit
             </button>
           </div>
         </div>
       </form>
     </div>
    <body>
	
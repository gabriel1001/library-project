<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class books extends Model
{
	 public function borrowers() 
	{ 
		return $this->belongsTo('App\Borrower','books_id'); 
	}
	public function borrowing() 
	{ 
		return $this->belongsToMany('App\Category'); 
	}
    use SoftDeletes;
	
	protected $dates = ['deleted_at']; //
}

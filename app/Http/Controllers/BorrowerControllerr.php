<?php

namespace App\Http\Controllers;
use App\Borrower;
use Illuminate\Http\Request;

class BorrowerControllerr extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $borrowers = \App\Borrower::all();
      return view('borrowers.index', compact('borrowers')); //  //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('borrowers.create');  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $borrowers= new \App\Borrower;
	  $borrowers->name=$request->get('name');
      $borrowers->icnumber=$request->get('icnumber');
      $borrowers->address=$request->get('address');
	  $borrowers->phonenumber=$request->get('phonenumber');
	  $borrowers->save();
      return redirect('borrowers')->with('success', 'Borrower has been added');   //  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $borrowers = Borrower::where('id',$id)->firstOrFail();
      return view('borrowers.edit',compact('borrowers'));//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $borrowers= \App\Borrower::find($id);
	  $borrowers->name=$request->get('name');
      $borrowers->icnumber=$request->get('icnumber');
      $borrowers->address=$request->get('address');
	  $borrowers->phonenumber=$request->get('phonenumber');
	  $borrowers->save();
      return redirect('borrowers')->with('success', 'Borrowers has been updated');   //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function destroy(borrower $borrowers)
    {
      $name = $borrowers->name;
      $borrowers->delete();
      return redirect('books')->with('success','books '.$name.' has been deleted');  //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
		return view('admin');
    }

	/**
	admin things
	*/
	public function admin()
	{
		if (Gate::allows('admin-only', auth()->user())) {
		return view('admin');
		}
		return 'Restrictied Area: Admin Only!';
	}
	
}

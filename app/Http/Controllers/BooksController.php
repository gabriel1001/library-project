<?php

namespace App\Http\Controllers;


use App\books;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $books = \App\books::all();
      return view('books.index', compact('books')); //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('books.create');  //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $books= new \App\books;
	  $books->bookid=$request->get('bookid');
      $books->title=$request->get('title');
      $books->author=$request->get('author');
	  $books->publishername=$request->get('publishername');
	  $books->publishedyear=$request->get('publishedyear');
	  $books->category=$request->get('category');
	  $books->save();
      return redirect('books')->with('success', 'Books has been added');  //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $books = books::where('id',$id)->firstOrFail();
      return view('books.edit',compact('books'));//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $books= \App\books::find($id);
	  $books->bookid=$request->get('bookid');
      $books->title=$request->get('title');
      $books->author=$request->get('author');
	  $books->publishername=$request->get('publishername');
	  $books->publishedyear=$request->get('publishedyear');
	  $books->category=$request->get('category');
	  $books->save();
      return redirect('books')->with('success', 'Books has been updated'); //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(books $books)
    {
      $name = $books->name;
      $books->delete();
      return redirect('books')->with('success','books '.$name.' has been deleted');  //
    }
}

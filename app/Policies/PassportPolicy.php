<?php

namespace App\Policies;

use App\User;
use App\Passport;
use Illuminate\Auth\Access\HandlesAuthorization;

class PassportPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the passport.
     *
     * @param  \App\User  $user
     * @param  \App\Passport  $passport
     * @return mixed
     */
    public function view(User $user, Passport $passport)
    {
        //
    }

    /**
     * Determine whether the user can create passports.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the passport.
     *
     * @param  \App\User  $user
     * @param  \App\Passport  $passport
     * @return mixed
     */
    public function update(User $user, Passport $passport)
    {
        //
    }

    /**
     * Determine whether the user can delete the passport.
     *
     * @param  \App\User  $user
     * @param  \App\Passport  $passport
     * @return mixed
     */
    public function delete(User $user, Passport $passport)
    {
        //
    }

    /**
     * Determine whether the user can restore the passport.
     *
     * @param  \App\User  $user
     * @param  \App\Passport  $passport
     * @return mixed
     */
    public function restore(User $user, Passport $passport)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the passport.
     *
     * @param  \App\User  $user
     * @param  \App\Passport  $passport
     * @return mixed
     */
    public function forceDelete(User $user, Passport $passport)
    {
        //
    }
}
